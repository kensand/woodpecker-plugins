#!/usr/bin/env sh

if [ -z "$PLUGIN_FILE" ]
then
  echo "ERROR: Missing 'file' setting."
  exit 1
fi

if [ -z "$PLUGIN_USERNAME" ]
then
  echo "ERROR: Missing 'username' setting."
  exit 1
fi

if [ -z "$PLUGIN_PASSWORD" ]
then
  echo "ERROR: Missing 'password' setting."
  exit 1
fi

if [ -n "$PLUGIN_TAG" ]
then
    TAG=$PLUGIN_TAG
elif [ -n "$CI_COMMIT_TAG" ]
then
    TAG=$CI_COMMIT_TAG
else
    TAG="latest"
fi

if [ -z "$PLUGIN_HOST" ]
then
    BASE_URL=$CI_FORGE_URL
else
    BASE_URL=$PLUGIN_HOST
fi

FILENAME=$(basename "$PLUGIN_FILE")
PACKAGE_URL=$BASE_URL/api/packages/$CI_REPO_OWNER/generic/$CI_REPO_NAME/$TAG/$FILENAME

curl --user "$PLUGIN_USERNAME":"$PLUGIN_PASSWORD" -X DELETE \
     "$PACKAGE_URL"

echo "Uploading to $PACKAGE_URL"

curl --user "$PLUGIN_USERNAME":"$PLUGIN_PASSWORD" \
     --upload-file "$PLUGIN_FILE" \
     "$PACKAGE_URL"