# Woodpecker Plugin Containers

![CI/CD Pipeline Status](https://woodpecker-codeberg.kensand.net/api/badges/6/status.svg)

Various development environment containers.

# Containers 
Container Name | Image | Notes
---|---|---
[Forgejo Generic Package](https://codeberg.org/kensand/-/packages/container/woodpecker-plugins-forgejo-generic-package/latest) | codeberg.org/kensand/woodpecker-plugins-forgejo-generic-package:latest | Upload files to Forgejo as [generic packages](https://forgejo.org/docs/latest/user/packages/generic/)